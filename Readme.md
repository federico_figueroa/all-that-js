# All that JS
## Introducción a Javascript

**Nota:** _Esta presentación se hizo hace un tiempo y algunos de los datos proporcionados puede que ya no sean válidos._

**Nota:** _La presentación menciona que se habla sobre AngularJS pero este tema es tan grande que fue separado y se encuentra en un documento fuera de este repositorio._

Presentación para una charla de introducción al lenguaje de programación **Javascript**.

La presentación fue generada con [Generator Reveal](https://github.com/slara/generator-reveal) y para poder utilizar todas las ventajas de [Reveal](http://lab.hakim.se/reveal-js/#/) debemos seguir el seguiente procedimiento:

1. Instalar Grunt si no se tiene aún
1. Clonar este repositorio
1. Instalar las dependencias
1. Ejecutar grunt quien lanzará el webserver
```bash
npm install -g grunt-cli
npm install
bower install
grunt
```
Esta presentación es acompañada de un documento, cuya versión más reciente puede ser encontrada [Aquí](https://docs.google.com/document/d/1zt9cjrXkIoYht0A386Xyli4zOmsKyn4xPesb2wUrz6Y/edit?usp=sharing).

### Todo:

1. Revisar completamente para encontrar errores o datos que ya no son válidos
1. Actualizar los datos desactualizados
1. Incluír los cambios introducidos por ES6 o ligar esta presentación a una nueva
